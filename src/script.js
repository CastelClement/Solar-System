import './style.css'
import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import { CSS2DRenderer, CSS2DObject } from 'three/examples/jsm/renderers/CSS2DRenderer.js'
import { Raycaster, Vector2, Vector3, Box3 } from 'three'
import * as dat from 'dat.gui'

// Loading
const textureLoader = new THREE.TextureLoader()

// Debug
const gui = new dat.GUI()

// Canvas
const canvas = document.querySelector('canvas.webgl')

// Scene
const scene = new THREE.Scene()

// Objects
const sunGeo = new THREE.SphereGeometry(16, 30, 30);

// Materials
const backgroundLoader = new THREE.CubeTextureLoader();
scene.background = backgroundLoader.load(["/textures/stars.jpg","/textures/stars.jpg","/textures/stars.jpg","/textures/stars.jpg","/textures/stars.jpg", "/textures/stars.jpg"])

const sunMat = new THREE.MeshBasicMaterial({
    map: textureLoader.load("/textures/sun.jpg")
});

// Mesh
const sun = new THREE.Mesh(sunGeo, sunMat);
sun.name = "SUN"
scene.add(sun);

// Label
const labelDiv = document.createElement('div');
labelDiv.className = 'label';
labelDiv.style.marginTop = '-1em';
const label = new CSS2DObject(labelDiv);
label.visible = false;
scene.add(label);

const raycaster = new Raycaster();
const mouse = new Vector2();

window.addEventListener('mousemove', ({ clientX, clientY }) => {
    const { innerWidth, innerHeight } = window;

    mouse.x = (clientX / innerWidth) * 2 - 1;
    mouse.y = -(clientY / innerHeight) * 2 + 1;
});





// CREATING PLANETS
function createPlanet(name, radius, texture, pos, ring) {
    // Objet
    const planetGeo = new THREE.SphereGeometry(radius, 30, 30);

    // Material
    const planetMat = new THREE.MeshStandardMaterial({
        map: texture
    });

    // Mesh
    const mesh = new THREE.Mesh(planetGeo, planetMat);
    mesh.name = name;
    const obj = new THREE.Object3D();
    obj.add(mesh);

    if(ring) {
        // Object
        const ringGeo = new THREE.RingGeometry(
            ring.innerRadius,
            ring.outerRadius,
            32);

        // Material
        const ringMat = new THREE.MeshBasicMaterial({
            map: ring.texture,
            side: THREE.DoubleSide
        });

        // Mesh
        const ringMesh = new THREE.Mesh(ringGeo, ringMat);
        obj.add(ringMesh);

        ringMesh.position.x = pos;
        ringMesh.rotation.x = -0.5 * Math.PI;
    }

    scene.add(obj);
    mesh.position.x = pos;
    return {mesh, obj}
}

const mercury = createPlanet("Mercury", 3.2, textureLoader.load("/textures/mercury.jpg"), 28);
const venus = createPlanet("Venus", 5.8, textureLoader.load("/textures/venus.jpg"), 44);
const earth = createPlanet("Earth", 6, textureLoader.load("/textures/earth.jpg"), 62);
const mars = createPlanet("Mars", 4, textureLoader.load("/textures/mars.jpg"), 78);
const jupiter = createPlanet("Jupiter", 12, textureLoader.load("/textures/jupiter.jpg"), 100);
const saturn = createPlanet("Saturn", 10, textureLoader.load("/textures/saturn.jpg"), 138, {
    innerRadius: 10,
    outerRadius: 20,
    texture: textureLoader.load("/textures/saturn\ ring.png")
});
const uranus = createPlanet("Uranus", 7, textureLoader.load("/textures/uranus.jpg"), 176, {
    innerRadius: 7,
    outerRadius: 12,
    texture: textureLoader.load("/textures/uranus\ ring.png")
});
const neptune = createPlanet("Neptune", 7, textureLoader.load("/textures/neptune.jpg"), 200);
const pluto = createPlanet("Pluto", 2.8, textureLoader.load("/textures/pluto.jpg"), 216);



// Lights
const ambientLight = new THREE.AmbientLight(0x333333);
scene.add(ambientLight);

const pointLight = new THREE.PointLight(0xFFFFFF, 2, 300);
scene.add(pointLight);


// GUI
var anim = {playAnim: true, speed: 1.0}
gui.add(anim, "playAnim")
gui.add(anim, "speed").min(0).step(0.01).listen()


/**
 * Sizes
 */
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}

window.addEventListener('resize', () =>
{
    // Update sizes
    sizes.width = window.innerWidth
    sizes.height = window.innerHeight

    // Update camera
    camera.aspect = sizes.width / sizes.height
    camera.updateProjectionMatrix()

    // Update renderer
    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
})

/**
 * Camera
 */
// Base camera
const camera = new THREE.PerspectiveCamera(45, sizes.width / sizes.height, 0.1, 1000)
camera.position.x = -90
camera.position.y = 140
camera.position.z = 140
scene.add(camera)

// Controls
const controls = new OrbitControls(camera, canvas)
controls.update()

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
    canvas: canvas,
    antialias: true
})
renderer.setSize(sizes.width, sizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

const labelRenderer = new CSS2DRenderer();
labelRenderer.setSize(innerWidth, innerHeight);
labelRenderer.domElement.style.position = 'absolute';
labelRenderer.domElement.style.top = '0px';
labelRenderer.domElement.style.pointerEvents = 'none';
document.body.appendChild(labelRenderer.domElement);

/**
 * Animate
 */
var keyheld = 0
var previousSpeed = -1
window.addEventListener("keydown", (e) => {
    console.log(e.code);
    if (e.code == "ShiftLeft") {
        keyheld = 0.01
    } else if (e.code == "ControlLeft") {
        keyheld = -0.01
    } else if (e.code == "KeyR") {
        anim.speed = 1
    } else if (e.code == "KeyS") {
        if (anim.speed != 0) {
            previousSpeed = anim.speed
            anim.speed = 0
        } else {
            anim.speed = previousSpeed
        }
    }
})
window.addEventListener("keyup", () => {
    keyheld = 0
})

const clock = new THREE.Clock()

const tick = () =>
{

    const elapsedTime = clock.getElapsedTime()

    anim.speed += keyheld
    console.log(anim.speed)

    // Update objects
    if (anim.playAnim) {
        //Self-rotation
        sun.rotateY(0.004*anim.speed);
        mercury.mesh.rotateY(0.004*anim.speed);
        venus.mesh.rotateY(0.002*anim.speed);
        earth.mesh.rotateY(0.02*anim.speed);
        mars.mesh.rotateY(0.018*anim.speed);
        jupiter.mesh.rotateY(0.04*anim.speed);
        saturn.mesh.rotateY(0.038*anim.speed);
        uranus.mesh.rotateY(0.03*anim.speed);
        neptune.mesh.rotateY(0.032*anim.speed);
        pluto.mesh.rotateY(0.008*anim.speed);

        //Around-sun-rotation
        mercury.obj.rotateY(0.04*anim.speed);
        venus.obj.rotateY(0.015*anim.speed);
        earth.obj.rotateY(0.01*anim.speed);
        mars.obj.rotateY(0.008*anim.speed);
        jupiter.obj.rotateY(0.002*anim.speed);
        saturn.obj.rotateY(0.0009*anim.speed);
        uranus.obj.rotateY(0.0004*anim.speed);
        neptune.obj.rotateY(0.0001*anim.speed);
        pluto.obj.rotateY(0.00007*anim.speed);
    }
    

    // Update Orbital Controls
    // controls.update()

    raycaster.setFromCamera(mouse, camera);
    const [hovered] = raycaster.intersectObjects(scene.children, true);
    if (hovered) {
        // Setup label
        renderer.domElement.className = 'hovered';
        label.visible = true;
        labelDiv.textContent = hovered.object.name;
  
        // Get offset from object's dimensions
        const offset = new Vector3();
        new Box3().setFromObject(hovered.object).getSize(offset);
  
        // Move label over hovered element
        label.position.set(
            hovered.point.x,
            hovered.point.y,
            hovered.point.z
        );
      } else {
        // Reset label
        renderer.domElement.className = '';
        label.visible = false;
        labelDiv.textContent = '';
      }
    

    // Render
    renderer.render(scene, camera)
    labelRenderer.render(scene, camera)

    // Call tick again on the next frame
    window.requestAnimationFrame(tick)
}

tick()